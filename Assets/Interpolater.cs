using System.Collections.Generic;
using UnityEngine;

public class Interpolater : MonoBehaviour
{
    public List<Transform> transforms;
    public float duration = 10.0f;
    [Range(.1f,10)]
    public float pow = 1.0f;
    public float sinEcc = 0.0f;
    public AnimationCurve curve;

    // Update is called once per frame
    void Update()
    {
        //Get the positions
        //Vector3 p0 = transforms[0].position;
        //Vector3 p1 = transforms[1].position;
        //Vector3 p2 = transforms[2].position;

        //calculate u from current time
        float u = Time.time / duration;
        u = u % 1;
        //u = Mathf.Clamp01(u);
        //u = Mathf.PingPong(u, 1);
        //u = u * u * u;
        u = Mathf.Pow(u, pow);
        u += sinEcc * Mathf.Sin(2 * Mathf.PI * u);
        u *= curve.Evaluate(u);
        transform.position = InterpolateFromList(transforms, u);

        //Vector3 p01 = (1 - u) * p0 + u * p1;
        //Vector3 p12 = (1 - u) * p1 + u * p2;
        //Vector3 p02 = (1 - u) * p01 + u * p12;
        ////Vector3 p01 = Vector3.Lerp(p0, p1, u);
        //transform.position = p02;
    }

    public void OnDrawGizmos()
    {
        int numSegments = 32;
        Vector3 prevPt = transforms[0].position;
        for (int i = 1; i <= numSegments; i++)
        {
            float u = i / (float)numSegments;
            Vector3 currPt = InterpolateFromList(transforms, u);
            Gizmos.DrawLine(prevPt, currPt);
            prevPt = currPt;
        }
    }

    private Vector3 InterpolateFromList(List<Transform> tList, float u)
    {
        //Interpolate
        //Array-Based (DOD) Solution
        int numPoints = tList.Count;
        Vector3[,] ptArray = new Vector3[numPoints, numPoints];

        //fill the top row with the points
        for (int i = 0; i < numPoints; i++)
        {
            ptArray[numPoints - 1, i] = tList[i].position;
        }

        //iterate from the top down, interpolating
        for (int r = numPoints - 2; r >= 0; r--)
        {
            for (int c = 0; c <= r; c++)
            {
                ptArray[r, c] = (1 - u) * ptArray[r + 1, c]
                    + u * ptArray[r + 1, c + 1];
            }
        }

        return ptArray[0, 0];
    }
}
